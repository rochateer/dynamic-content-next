const api = {

  get : async function (url : string) {
    try{
      const response = await fetch(
        url, 
        {
          headers : {
            "Content-Type": "application/json",
            "Authorization" : "Basic RGNBdXRoQWRtaW46QjlzZUlwM0kxdEdyNEl3NQ=="
          }
        });
      return response
    
    }catch(error){
      
      return error

    }
  }
}

export {
  api
}