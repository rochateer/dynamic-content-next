import { Group, Code, ScrollArea, rem, Text, UnstyledButton, Box, ThemeIcon, Collapse, SegmentedControl, Grid, TextInput } from '@mantine/core';
import {
  IconNotes,
  IconCalendarStats,
  IconGauge,
  IconPresentationAnalytics,
  IconFileAnalytics,
  IconAdjustments,
  IconLock,
  IconChevronRight,
} from '@tabler/icons-react';
import { Logo } from './Logo';
import classes from './Sidebar.module.css';
import React, { useState } from 'react';
import { MENU } from './Menus';
import Link from 'next/link';
// import { ComponentDynamicFormContext } from '../context/ComponentDynamicContentContext';
// import { ComponentDfContextType } from '../@types/component-df';
// import SidebarFormCreation from './SidebarFormCreation';

interface LinksGroupProps {
    icon: React.FC<any>;
    label: string;
    initiallyOpened?: boolean;
    link?: string,
    links?: { label: string; link: string }[];
}

export function LinksGroup({ icon: Icon, label, link, initiallyOpened, links }: LinksGroupProps) {
  const isNavMenu = link;
  const hasLinks = Array.isArray(links);
  const [opened, setOpened] = useState(initiallyOpened || false);
  const items = (hasLinks ? links : []).map((link, idx) => (
    <Link href={link.link} key={idx}>
      <Text
        className={classes.link}
        key={link.label}
      >
        {link.label}
      </Text>
    </Link>
  ));

  const NavMenu = ({link, children} : {link? : string, children : React.ReactElement}) => (
    isNavMenu && link ? (
      <Link href={link}>{children}</Link>
    ) : (
      <>{children}</>
    )
  )

  return (
    <>
      <NavMenu link={link}>
        <>
          <UnstyledButton onClick={() => setOpened((o) => !o)} className={classes.control}>
            <Group justify="space-between" gap={0}>
              <Box style={{ display: 'flex', alignItems: 'center' }}>
                <ThemeIcon variant="light" size={30}>
                  <Icon style={{ width: rem(18), height: rem(18) }} />
                </ThemeIcon>
                <Box ml="md">{label}</Box>
              </Box>
              {hasLinks && (
                <IconChevronRight
                  className={classes.chevron}
                  stroke={1.5}
                  style={{
                    width: rem(16),
                    height: rem(16),
                    transform: opened ? 'rotate(-90deg)' : 'none',
                  }}
                />
              )}
            </Group>
          </UnstyledButton>
          {hasLinks ? <Collapse in={opened}>{items}</Collapse> : null}
        </>
      </NavMenu>
    </>
  );
}

export function Sidebar(props : {type? : string}) {
  const links = MENU.map((item) => <LinksGroup {...item} key={item.label} />);

  const [section, setSection] = useState<string>('Menu');
  const [active, setActive] = useState('Billing');
//   const [currentComponent, setCurrentComponent] = useState({});

//   if(props.type === 'COMPONENT_CREATION'){
//     const {componentDf, activeSectionSidebar} = React.useContext(ComponentDynamicFormContext) as ComponentDfContextType;

//     console.log("componentDf", componentDf)

//     React.useEffect(() => {
//       if(activeSectionSidebar){
//         setSection(activeSectionSidebar)
//       }
//       if(componentDf){
//         setCurrentComponent(componentDf)
//       }
//     }, [componentDf?.id])
//   }

  return (
    <React.Fragment>
    {/* <nav className={classes.navbar}> */}
      <div className={classes.header}>
        <Group justify="space-between">
          <Logo style={{ width: rem(120) }}/>
          <Code fw={700}>v0.1.0</Code>
        </Group>
      </div>

      <SegmentedControl
        value={section}
        onChange={(value: any) => setSection(value)}
        transitionTimingFunction="ease"
        fullWidth
        data={props.type === "COMPONENT_CREATION" ? [
          { label: 'Menu', value: 'Menu' },
          { label: 'Form', value: 'Form' },
        ] : [
          { label: 'Menu', value: 'Menu' }
        ]}
      />

      <ScrollArea className={classes.links} mt="md">
        <div className={classes.linksInner}>{links}</div>
      </ScrollArea>

      <div className={classes.footer}>
      </div>
    {/* </nav> */}
    </React.Fragment>
  );
}