import { IconAdjustments, IconCalendarStats, IconFileAnalytics, IconGauge, IconLock, IconLogin, IconNotes, IconPresentationAnalytics, IconUsers } from "@tabler/icons-react";

export const MENU = [
    { label: 'Dashboard', icon: IconGauge, link : '/dashboard' },
    { label: 'Login', icon: IconLogin, link : '/login' },
    {
      label: 'System Management',
      icon: IconUsers,
      initiallyOpened: false,
      links: [
        { label: 'User Management', link: '/user-management' },
      ],
    }
  ]