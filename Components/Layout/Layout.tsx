'use client'

import React from 'react'
import { Sidebar } from './Sidebar'
import { AppShell} from '@mantine/core';
import { useDisclosure } from '@mantine/hooks';
import { Header } from './Header';
import './Layout.css'

function Layout(props : {children : React.ReactNode, type? : string}) {

    const [opened, { toggle }] = useDisclosure();  

    return (
        <React.Fragment>
            <AppShell
                header={{ height: { base: 60, sm: 60, lg: 60 } }}
                navbar={{
                    width: 250,
                    breakpoint: 'xs',
                    collapsed: { mobile: !opened },
                }}
                padding="md"
            >
                <AppShell.Header>
                    <Header opened={opened} toggle={toggle}/>
                </AppShell.Header>

                <AppShell.Navbar p="lg" pt="xs">
                    <Sidebar type={props.type} />
                </AppShell.Navbar>

                <AppShell.Main>
                    {props.children}
                </AppShell.Main>

            </AppShell>
        </React.Fragment>
    )
}

export default Layout;
