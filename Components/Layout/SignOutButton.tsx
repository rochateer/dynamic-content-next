'use client'

import React from 'react'
import { signOut } from 'next-auth/react'
import { Menu, rem } from '@mantine/core'
import { IconLogout } from '@tabler/icons-react'

function SignOutButton() {
  return (
    <>
      <Menu.Item
        onClick={() => signOut()}
        leftSection={
          <IconLogout style={{ width: rem(16), height: rem(16) }} stroke={1.5} />
        }
      >
        Logout
      </Menu.Item>
    </>
  )
}

export default SignOutButton