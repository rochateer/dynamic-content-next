import CardPaper from "./CardPaper"
import CardPaperBody from "./CardPaperBody"
import CardPaperHeader from "./CardPaperHeader"
import CardPaperTitle from "./CardPaperTitle"

export {
    CardPaper,
    CardPaperBody,
    CardPaperHeader,
    CardPaperTitle
}