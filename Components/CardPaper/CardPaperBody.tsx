import { Box } from '@mantine/core'
import React from 'react'

function CardPaperBody({children, style, className} : {children? : React.ReactNode, style? : React.CSSProperties | undefined, className? : string | undefined}) {
  return (
    <Box style={{...style}} pb={'xl'} className={className}>{children}</Box>
  )
}

export default CardPaperBody