import NextAuth, { DefaultSession } from 'next-auth';
import { User, Session } from 'next-auth';
import { DefaultJWT } from 'next-auth/jwt';

declare module 'next-auth' {
  /**
   * Returned by `useSession`, `getSession` and received as a prop on the `SessionProvider` React Context
   */
  interface Session extends DefaultSession {
    // gqlToken: string;
    accessToken: string;
    // refreshToken: string;
  }
}

declare module 'next-auth/jwt' {
  interface JWT extends DefaultJWT {
    // gqlToken: string;
    accessToken: string;
    // refreshToken: string;
  }
}