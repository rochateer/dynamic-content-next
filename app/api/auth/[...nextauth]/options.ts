// next-auth.config.ts

import NextAuth, { NextAuthOptions, User } from 'next-auth';
import CredentialsProvider from 'next-auth/providers/credentials';
import { jwtDecode } from "jwt-decode";

// Define a type for the user object returned by your authentication API
interface AuthUser {
  id: number;
  email: string;
  // Add other properties as needed
}

export const authOptions : NextAuthOptions = {
  session: {
    strategy: "jwt",
    maxAge: 3000,
  },
  pages: {
    signIn: "/login",
  },
  providers: [
    CredentialsProvider({
      // The name to display on the sign in form
      name: 'Credentials',
      credentials: {
        // Email and password fields
        email: { label: 'Email', type: 'text' },
        password: { label: 'Password', type: 'password' },
      },
      async authorize(credentials: any) {

        console.log("credentials", credentials)

        const backendCrederntials = {
          email : credentials.email,
          password : credentials.password
        }
        
        try {
          // Make a request to your authentication API to validate the credentials
          const response : any = await fetch('https://digitalprojectmanagement-api.ericsson.net/dc-auth-api/auth/login', {
            method: 'POST',
            headers: {
              'Content-Type': 'application/json',
            },
            body: JSON.stringify({...backendCrederntials}),
          });

          const resJson : any = await response.json();

          console.log(resJson)

          if (resJson.code === 400) {
            throw new Error('Authentication failed');
          }

          const user : any = resJson

          if(!user || !user.user){
            throw new Error('Authentication failed');
          }

          const tokenUser = user?.tokens?.access?.token

          const decoded = jwtDecode(tokenUser);

          console.log("jwt", decoded);

          return {
            id: user?.user?.email,
            email : user?.user?.email,
            name : user?.user?.name,
            token : tokenUser,
            accessToken : tokenUser,
            ...decoded
          };

        } catch (error) {
          // If authentication fails, return null
          console.error('Login failed:', error);
          return Promise.resolve(null);
        }
      },
    }),
    // Add other providers as needed
  ],
  callbacks: {
    jwt: async ({ token, user } : {token : any, user : any}) => {
      if (user) {
        token.email = user.email;
        token.username = user.userName;
        token.user_type = user.userType;
        token.accessToken = user.token;
      }

      return token;
    },
    session: ({ session, token, user } : {session : any, token : any, user : any}) => {
      if (token) {
        session.user.email = token.email;
        session.user.username = token.userName;
        session.user.accessToken = token.accessToken;
      }
      return user;
    },
    async redirect({ url, baseUrl }) {
      // Allows relative callback URLs
      if (url.startsWith("/")) return `${baseUrl}/dashboard`
      // Allows callback URLs on the same origin
      // else if (new URL(url).origin === baseUrl) return url
      return `${baseUrl}/dashboard`
    }
  },
  secret: process.env.NEXTAUTH_SECRET
};
