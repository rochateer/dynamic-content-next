import NextAuth from "next-auth"
import { encode, decode } from "next-auth/jwt"
import CredentialsProvider from "next-auth/providers/credentials"
import Cookies from "cookies"

const session = {
  // strategy: "database",
  maxAge: 30 * 24 * 60 * 60, // 30 days
  updateAge: 24 * 60 * 60, // 24 hours
}

export const authOptions = (req, res) => {
  return {
    session: {
      strategy: "jwt",
      maxAge: 3000,
    },
    pages: {
      signIn: "/login",
    },
    providers: [
      CredentialsProvider({
        // The name to display on the sign in form
        name: 'Credentials',
        credentials: {
          // Email and password fields
          email: { label: 'Email', type: 'text' },
          password: { label: 'Password', type: 'password' },
        },
        async authorize(credentials) {

          console.log("credentials", credentials)

          const backendCrederntials = {
            email : credentials.email,
            password : credentials.password
          }
          
          try {
            // Make a request to your authentication API to validate the credentials
            const response = await fetch('https://digitalprojectmanagement-api.ericsson.net/dc-auth-api/auth/login', {
              method: 'POST',
              headers: {
                'Content-Type': 'application/json',
              },
              body: JSON.stringify({...backendCrederntials}),
            });

            const resJson = await response.json();

            console.log(resJson)

            if (resJson.code === 400) {
              throw new Error('Authentication failed');
            }

            const user = resJson

            if(!user || !user.user){
              throw new Error('Authentication failed');
            }

            const tokenUser = user?.tokens?.access?.token

            const decoded = jwtDecode(tokenUser);

            console.log("jwt", decoded);

            return {
              id: user?.user?.email,
              email : user?.user?.email,
              name : user?.user?.name,
              token : tokenUser,
              accessToken : tokenUser,
              ...decoded
            };

          } catch (error) {
            // If authentication fails, return null
            console.error('Login failed:', error);
            return Promise.resolve(null);
          }
        },
      }),
      // Add other providers as needed
    ],
    callbacks: {
      session({ session, user }) {
        console.log("SESSION callback", session, user)
        if (session.user) {
          session.user.id = user.id
        }
        return session
      },
      async signIn({ user }) {
        if (
          req.query.nextauth?.includes("callback") &&
          req.query.nextauth?.includes("credentials") &&
          req.method === "POST"
        ) {
          if (user && "id" in user) {
            const sessionToken = user?.tokens?.access?.token
            const sessionExpiry = new Date(Date.now() + session.maxAge * 1000)
            await adapter.createSession({
              sessionToken : sessionToken,
              userId: user.id,
              user : {
                name : user.name,
                email : user.email
              },
              expires: sessionExpiry,
              // userAgent: req.headers["user-agent"] ?? null,
            })
            const cookies = new Cookies(req, res)
            cookies.set("next-auth.session-token", sessionToken, {
              expires: sessionExpiry,
            })
          }
        }
        return true
      },
    },
    
    
    //needs to override default jwt behavior when using Credentials 
    jwt: {
      encode(params) {
        if (
          req.query.nextauth?.includes("callback") &&
          req.query.nextauth?.includes("credentials") &&
          req.method === "POST"
        ) {
          const cookies = new Cookies(req, res)
          const cookie = cookies.get("next-auth.session-token")
          if (cookie) return cookie
          else return ""
        }
        // Revert to default behaviour when not in the credentials provider callback flow
        return encode(params)
      },
      async decode(params) {
        if (
          req.query.nextauth?.includes("callback") &&
          req.query.nextauth?.includes("credentials") &&
          req.method === "POST"
        ) {
          return null
        }
        // Revert to default behaviour when not in the credentials provider callback flow
        return decode(params)
      },
    },
    adapter,
    session,
  }
}
export default async function auth(req, res) {
  // Do whatever you want here, before the request is passed down to `NextAuth`
  return await NextAuth(req, res, authOptions(req, res))
}