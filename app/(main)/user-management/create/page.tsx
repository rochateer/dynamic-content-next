import React from 'react'
import { CardPaper, CardPaperBody, CardPaperHeader, CardPaperTitle } from '../../../../Components/CardPaper'
import { MultiSelect, TextInput } from '@mantine/core'

function Create() {
  return (
    <section>
      <CardPaper>
        <CardPaperHeader>
          <CardPaperTitle>User Creation</CardPaperTitle>
        </CardPaperHeader>
        <CardPaperBody>
          <TextInput name="name" placeholder="Name" label="Name" />
          <MultiSelect
            label="Role"
            name="role"
            placeholder="please select user role"
            searchable
            mt={"sm"}
            data={['Admin', 'Engineer', 'Developer']}
          />
        </CardPaperBody>
      </CardPaper>
    </section>
  )
}

export default Create