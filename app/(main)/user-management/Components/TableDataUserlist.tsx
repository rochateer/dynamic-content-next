'use client'

import { Table } from '@mantine/core'
import React from 'react'
import { TablePagination } from '../../../../Components/Table'

function TableDataUserlist(props : {userData : any}) {

  const handleOnChange = () => {}

  return (
    <section>
      <Table stickyHeader stickyHeaderOffset={60} withTableBorder highlightOnHover>
        <Table.Thead>
          <Table.Tr>
            <Table.Th>Client Name</Table.Th>
            <Table.Th>Roles</Table.Th>
            <Table.Th>Account</Table.Th>
          </Table.Tr>
        </Table.Thead>
        <Table.Tbody>
          {props.userData.map((user : any) =>
            <Table.Tr key={user._id}>
              <Table.Td>{user.client_app}</Table.Td>
              <Table.Td>{user.roles.join(", ")}</Table.Td>
              <Table.Td>{user.accounts.join(", ")}</Table.Td>
            </Table.Tr>
          )}
        </Table.Tbody>
        <Table.Caption>
          <TablePagination 
            totalCurrentData={props.userData.length}
            activePage={1}
            totalData={1}
            onChange={handleOnChange}
          />
        </Table.Caption>
      </Table>     
    </section>
  )
}

export default TableDataUserlist