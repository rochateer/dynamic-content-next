import React from 'react'
// import { CardPaper, CardPaperBody, CardPaperHeader, CardPaperTitle } from '../../Components/CardPaper'
import { Box, Button, Flex, Table, Text } from '@mantine/core'
import { useQuery } from '@tanstack/react-query';
import { CardPaper, CardPaperBody, CardPaperHeader, CardPaperTitle } from '../../../Components/CardPaper';
import { TablePagination } from '../../../Components/Table';
import TableDataUserlist from './Components/TableDataUserlist';
import Link from 'next/link';

const fetchUsers = async (activePage : number = 1, limit : number = 10) => {
  const SKIP = (activePage-1)*limit
  
  try{

    const res = await fetch(
      'https://digitalprojectmanagement-api.ericsson.net/dc-auth-api/client?limit'+limit+'&skip='+SKIP, {
        headers : {
          Authorization : "Basic RGNBdXRoQWRtaW46QjlzZUlwM0kxdEdyNEl3NQ=="
        }
      });
    return res

  }catch(error){
    
    return undefined
  }
  
};

async function UserManagement() {

  let data

  const resData = await fetchUsers();

  if(resData){
    data = await resData.json()
  }

  return (
    <section>
      <CardPaper>
        <CardPaperHeader>
          <div style={{display : 'flex', justifyContent : 'space-between'}}>
            <CardPaperTitle>User Management</CardPaperTitle>
            <Link className="nav-link" href="/user-management/create">
              <Button size="xs" radius="xs">Create</Button>
            </Link>
          </div>
          
        </CardPaperHeader>
        <CardPaperBody>

          <TableDataUserlist userData={data.data} />
          
          {/* <Table stickyHeader stickyHeaderOffset={60} withTableBorder highlightOnHover>
            <Table.Thead>
              <Table.Tr>
                <Table.Th>Email</Table.Th>
                <Table.Th>First Name</Table.Th>
                <Table.Th>Last Name</Table.Th>
                <Table.Th>Username</Table.Th>
                <Table.Th style={{textAlign : 'center'}}>Age</Table.Th>
              </Table.Tr>
            </Table.Thead>
            <Table.Tbody>
              {rows}
            </Table.Tbody>
            <Table.Caption>
              
              <TablePagination totalData={data && data.total} totalCurrentData={data?.users?.length} limitData={10} activePage={activePage} onChange={setActivePage} />
              {elements.length} from {elements.length} data
            </Table.Caption>
          </Table> */}
        </CardPaperBody>
      </CardPaper>
    </section>
  )
}

export default UserManagement