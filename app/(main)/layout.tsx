import "@mantine/core/styles.css";
import React from "react";
import Layout from "../../Components/Layout/Layout";

export default function MainLayout({ children }: { children: any }) {
  return (
    <>
      <Layout>
        {children}
      </Layout>
    </>
  );
}
