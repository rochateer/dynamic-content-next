import { getServerSession } from 'next-auth';
import React from 'react'
import { authOptions } from '../../api/auth/[...nextauth]/options';

const Dashboard = async () => {
  const session = await getServerSession(authOptions);
  console.log("session dashboard", session);
  return (
    <div>Dashboard Test</div>
  )
}

export default Dashboard