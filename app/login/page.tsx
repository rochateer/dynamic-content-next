'use client'

import {
  Paper,
  TextInput,
  PasswordInput,
  Checkbox,
  Button,
  Title,
  Text,
  Anchor,
} from '@mantine/core';
import classes from './authSignin.module.css';
import { HTMLAttributes, useState } from 'react';
import { signIn } from 'next-auth/react';
import { useRouter } from 'next/navigation';

export default function Login() {

  const [email, setEmail] = useState('fake@example.com');
  const [password, setPassword] = useState('password1');
  const [error, setError] = useState('');

  const router = useRouter();

  const handleSubmitCredentials = async (e : any) => {

    e.preventDefault();

    try {
      // Make a request to sign in using the provided credentials

      const response: any = await signIn('credentials', { email, password });

      console.log({ response });
      // if (response) {
      //   router.push("/dashboard");
      //   router.refresh();
      // }

      console.log("Login Successful", response);

      // if (!response) {
      //   throw new Error("Network response was not ok");
      // }
      // Process response here

      // router.push(Array.isArray(from) ? from[0] : from || '/dashboard');

      // Redirect user to dashboard or home page on successful login
    } catch (error) {
      // If login fails, display error message
      setError('Invalid email or password');
      console.error('Login failed:', error);
    }

  }

  return (
    <div className={classes.wrapper}>
      <Paper className={classes.form} radius={0} p={30}>
        <Title order={2} className={classes.title} ta="center" mt="md" mb={50}>
          Welcome back to DyCon!
        </Title>

        <TextInput label="Email address" onChange={(e) => setEmail(e.target.value)} placeholder="hello@gmail.com" size="md" />
        <PasswordInput label="Password" onChange={(e) => setPassword(e.target.value)} placeholder="Your password" mt="md" size="md" />
        <Checkbox label="Keep me logged in" mt="xl" size="md" />
        <Button fullWidth mt="xl" size="md" onClick={(e) => handleSubmitCredentials(e)}>
          Login
        </Button>

        <Text ta="center" mt="md">
          Don&apos;t have an account?{' '}
          <Anchor<'a'> href="#" fw={700} onClick={(event) => event.preventDefault()}>
            Register
          </Anchor>
        </Text>
      </Paper>
    </div>
  );
}